import React from 'react';

import {GlobalStyle} from "./styles/global";
import SessionContextProvider from "./store/Session/index";

import {ContainerApp} from "./components/ContainerApp";
import {CookiesProvider} from "./services/cookie";
import Routes from "./routes";

function App() {
    const user = {name: 'Tania', loggedIn: false};

    return (
        <CookiesProvider>
            <SessionContextProvider>
                <GlobalStyle />
                <Routes />
            </SessionContextProvider>
        </CookiesProvider>
    );
}

export default App;
