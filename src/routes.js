import React from "react";
import {
    BrowserRouter as Router,
    Route
} from 'react-router-dom';

import Login from "./pages/Login";
import Signup from "./pages/Signup";
import ForgotPassword from "./pages/ForgotPassword";

export default function Routes() {
    return (
        <Router>
            <Route exact path='/' component={Login}/>
            <Route exact path='/signup' component={Signup}/>
            <Route exact path='/forgot-password' component={ForgotPassword}/>
        </Router>
    );
}
