import api from 'axios';
require('dotenv').config();

console.log(process.env);

/**
 * Autentica com o Google
 */
export const GoogleAuthApi = async () => {
    const url = process.env.APP_ACL | null;
    if (!url) {
        return false;
    }

    const response = await api.get(process.env);
    console.log(response);
};

import axios from 'axios';

const findUser = async () => {
    return await axios.get('https://jsonplaceholder.typicode.com/todos');
};

export {
    findUser
};