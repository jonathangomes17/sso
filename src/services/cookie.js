import React from 'react';
import {CookiesProvider as CookieProvider, useCookies as useCookie} from 'react-cookie';

export const CookiesProvider = ({children}) => {
    return <CookieProvider> {children} </CookieProvider>;
};

export const useCookies = (name) => useCookie(name);
