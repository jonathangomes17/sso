import React, {createContext, useReducer, useEffect} from 'react';
import {sessionReducer} from './reducers';

export const SessionContext = createContext(null);

export default function SessionContextProvider(props) {

    const [session, setSession] = useReducer(sessionReducer, {}, () => {
        return {};
    });

    useEffect(() => {
        // TODO:
    }, [session]);

    return (
        <SessionContext.Provider value={{session, setSession}}>
            {props.children}
        </SessionContext.Provider>
    );
};
