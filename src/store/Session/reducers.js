import React from 'react';
import {useCookies} from "../../services/cookie";

export const sessionReducer = (state, action) => {
    //const [cookies, setCookie, removeCookie] = useCookies('_bl');

    switch (action.type) {
        case 'SET_SESSION':
            const {token} = action.session;
            //setCookie('_bl', token);
            return [...state, token];
        case 'GET_SESSION':
            const {cookie} = action.cookie;
            return [...state, cookie];
        case 'REMOVE_SESSION':
            //removeCookie('_bl');
            return state;
        default:
            return state;
    }
};
