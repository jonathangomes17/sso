import React from 'react';
import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    flex-direction: column;
`;

export const Card = styled.div`
    background-color: white;
    padding: 1.5rem;
    margin-top: 1rem;
`;

export const Link = styled.div`
    color: #006ace;
    text-decoration: none;
    background-color: transparent;
    outline: none;
    cursor: pointer;
    transition: color 0.3s;
`;

export const Header = styled.header`
    background: red;
    width: 100%;
    height: 40px;
`;

export const Col6 = styled.div`
    flex: 50%
`;
