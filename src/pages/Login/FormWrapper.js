import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Env from '../../config/environment';

import {
    Form,
    Icon,
    Input,
    Button,
    Link
} from 'drmanhattan';

import {Link as RouterLink } from 'react-router-dom';

const FormLogin = (props) => {
    const {getFieldDecorator} = props.form;

    function handleSubmit() {
    }

    function validateToNextPassword(rule, value, callback) {
        const { form } = props;
        form.validateFields(['confirm'], { force: true });
        callback();
    };

    return (
        <Form onSubmit={handleSubmit} className="login-form">
            <Form.Item>
                <Button size="large" block>
                    <FontAwesomeIcon icon={['fab', 'google']}/>
                    <Link href={Env.GOOGLE_ACL_URL} value="Acessar com o Google" rel="noopener noreferrer"/>
                </Button>
            </Form.Item>

            {/* Virar Componente */}
            <h1> Acessar </h1>

            <Form.Item>
                {getFieldDecorator('username', {
                    rules: [{required: true, message: 'Informe o e-mail'}],
                })(
                    <Input
                        size="large"
                        prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                        placeholder="E-mail"
                    />,
                )}
            </Form.Item>
            <Form.Item hasFeedback>
                {getFieldDecorator('password', {
                    rules: [
                        {
                            required: true,
                            message: 'Informe a senha',
                        },
                        {
                            validator: validateToNextPassword
                        },
                    ],
                })(
                    <Input.Password
                        size="large"
                        prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>}
                        type="password"
                        placeholder="Senha"
                    />
                )}
            </Form.Item>
            <Form.Item>
                <Button type="primary" size="large" htmlType="submit" className="login-form-button"
                        block> Acessar </Button>

                <RouterLink to="/forgot-password" value="Esqueci a minha senha">
                    Esqueci a minha senha
                </RouterLink>

                {/*<Button type="secondary" size="large" block> Novo acesso </Button>*/}
            </Form.Item>
        </Form>
    )
};

const FormWrapper = Form.create({name: 'normal_login'})(FormLogin);

export default FormWrapper;