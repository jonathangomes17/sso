import React from 'react';
import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    height: 100vh;
    align-items: center;
    justify-content: center;
`;

export const Info = styled.section`
    width: 100%;
    background-color: white;
    padding: 1.5rem;
    margin-top: 1rem;
    height: 10em;
    display: flex;
    justify-content: center;
    align-items: center;
    @media (min-width: 768px) {
        width: 400px;
    }
`;

export const Logo = styled.img`
    width: 100%;
    max-width: 352px;
    height: auto;
`;

export const Form = styled.section`
    width: 100%;
    background-color: white;
    padding: 1.5rem;
    margin-top: 1rem;
    @media (min-width: 768px) {
        width: 400px;
    }
`;

export const Link = styled.div`
    color: #006ace;
    text-decoration: none;
    background-color: transparent;
    outline: none;
    cursor: pointer;
    transition: color 0.3s;
`;

// export const Header = styled.header`
//     background: red;
//     width: 100%;
//     height: 40px;
// `;
//
// export const Col6 = styled.div`
//     flex: 50%
// `;
