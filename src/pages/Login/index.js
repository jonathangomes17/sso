import React, {useEffect} from 'react';
import logo from '../../assets/Logo-BulkyLog-By-MadeiraMadeira.png';

// TODO: Migrar para o DS @bulkylog/drmanhattan
import {
    Container,
    Info,
    Form,
    Logo
} from './styles';

import FormWrapper from "./FormWrapper";

export default function Login(props) {
    console.log("LOGIN");
    //const [cookies, setCookie] = useCookies(['_bl']);

    function refreshList() {
        console.log('Atualizar Lista!');
    }

    function userAuthenticate() {
        console.log('Autenticar o usuário!');

        // setCookie('_bl', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c', {
        //     path: '/',
        //     maxAge: '72000'
        // });
    }

    useEffect(() => {
        console.log('Montando!');

        return () => (
            console.log('Desmontando!')
        )
    });

    return (
        <Container>
            {/* TODO: Esperar o UX montar a tela para definir reuso! */}
            <Info>
                <Logo src={logo} alt="BulkyLog"/>
            </Info>
            <Form>
                <FormWrapper/>
            </Form>
        </Container>
    )
};
