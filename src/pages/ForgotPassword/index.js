import React from 'react';
import {
    Container,
    Info
} from './styles';

import {
    Form,
    Icon,
    Input,
    Button
} from 'drmanhattan';
import {Link as RouterLink} from "react-router-dom";

const FormLogin = (props) => {
    const {getFieldDecorator} = props.form;

    function handleSubmit() {
    }

    return (
        <Form onSubmit={handleSubmit}>
            <Form.Item>
                {getFieldDecorator('email', {
                    rules: [
                        {
                          type: 'email',
                          message: 'E-mail inválido'
                        },
                        {
                            required: true,
                            message: 'Informe o e-mail'
                        }
                    ],
                })(
                    <Input
                        size="large"
                        prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>}
                        placeholder="Email"
                    />
                )}
            </Form.Item>
            <Form.Item>
                <Button type="primary" size="large" htmlType="submit" className="login-form-button"
                        block> Solicitar </Button>
            </Form.Item>
        </Form>
    )
};

const FormWrapper = Form.create({name: 'normal_login'})(FormLogin);

export default function ForgotPassword(props) {
    return (
        <Container>
            <Info>
                <RouterLink to="/" value="Voltar para Login">
                    <Button>
                        <Icon type="arrow-left" style={{color: 'rgba(0,0,0,.25)'}}/>
                    </Button>
                </RouterLink>

                {/* TODO: Criar componente H1 no drmanhattan */}
                <h1> Recuperar senha </h1>

                <FormWrapper/>
            </Info>
        </Container>
    )
}