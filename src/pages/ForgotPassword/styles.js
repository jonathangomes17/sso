import React from 'react';
import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    height: 100vh;
    align-items: center;
    justify-content: center;
`;

export const Info = styled.section`
    width: 100%;
    background-color: white;
    padding: 1.5rem;
    margin-top: 1rem;
    @media (min-width: 768px) {
        width: 400px;
    }
`;
