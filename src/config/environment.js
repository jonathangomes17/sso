'use strict';

const production = {
    GOOGLE_ACL_URL: 'https://acl.bulkylog.com.br/acl-login/login/90e8feac250b7cd42fa6cf68d3be47cd3f6f25b0'
};

const development = {
    GOOGLE_ACL_URL: 'https://acl.bulkylog.com.br/acl-login/login/90e8feac250b7cd42fa6cf68d3be47cd3f6f25b0'
};

const Env = process.env.NODE_ENV === 'production' ? production : development;

export default Env;