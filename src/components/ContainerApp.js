import React, {useEffect} from 'react';
// import {EventEmitter} from "../services/eventEmitter";

import styled from 'styled-components';

const Container = styled.main`
    display: flex;
    flex-direction: column;
    padding: 1rem;
    font-size: 1.6666vw;
    height: 100vh;
`;

//
// function NotificationUser() {
//     console.log('Notifica usuário!');
// }
//
// function Recalcula() {
//     console.log('Recalcula');
// }

export const ContainerApp = (props) => {
    //
    // useEffect(() => {
    //     EventEmitter.on('NOTIFICATION', NotificationUser);
    //     EventEmitter.on('NOTIFICATION', Recalcula);
    // });

    return (
        <Container>
            {props.children}
        </Container>
    );
};