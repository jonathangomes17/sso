import React, {useEffect, useState} from 'react'

export default function(ComposedComponent, props) {
    console.log(props);
    const [isLogged, setIsLogged] = useState(false);

    useEffect(() => {
        // mount or unmount
    });

    return <ComposedComponent {...props} />
}