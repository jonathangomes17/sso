import { createGlobalStyle } from 'styled-components'

export const GlobalStyle = createGlobalStyle`
  body {
    background-color: #E5E5E5!important;
    padding: 0;
    margin: 0;
  }
`;
