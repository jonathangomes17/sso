export const colors = {
    primaryColor: "red",
    secondaryColor: "blue",
    tertiaryColor: "grey"
};

export const fonts = {
    Raleway: {
        fontFamily: "Raleway",
        fontStyle: "normal",
        fontWeight: "normal",
        src: "url('https://fonts.googleapis.com/css?family=Raleway') format('woff2')"
    },
    cool: {
        fontFamily: 'CoolFont',
        fontStyle: 'normal',
        fontWeight: 'normal',
        src: `url('coolfont.woff2') format('woff2'),
              url('coolfont.woff') format('woff')`
    }
};